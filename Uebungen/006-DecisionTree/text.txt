I.      Anpassung des Oberflächenmodelles
II.     Abfragen einer geeigneten Höhe für Gebäude
III.    Trennung von Vegetation und Gebäude
IV.     Maskierung der Gebäude im originalen Hyperspektralbild


?Wie groß ist die Bodenpixelauflösung des Oberflächenmodells?
    0,1m
?Welche durchschnittliche Bodenhöhe fällt Ihnen auf?
    60,44

!Anpassen des DOM:
    BandMath-> float(b1)-63

?Welche Bodenpixelauflösung besitzt diese Datei?
    "Metadata Viewer"->"Map Info"->"Pixel Size X/Y": 0,3m
?Wie viele spektrale Kanäle besitzt die Datei und in welchem Wellenlängenbereich liegen diese?
    
?Schauen Sie sich folgende Spektralprofil (Z-Profile) an: Gebäude, Straßen und Vegetation. Welche Unterschiede stellen Sie fest?
    "Nein, du sollst nur erkennen das man den NDVI über die beiden Kanäle NIR und Red bestimmen kann, weil hier ein Sprung in den Refektionseigenschaften ist."

!Berechnen Sie nun den NDVI mittels des Tools Band Math.
    (float(b1)-float(b2))/(float(b1)+float(b2)) b1: NIR, b2: RED
    60,40
    60,47

?Überlegen Sie sich durch welche Attribute sich ein Gebäude von den anderen Objekten mittels den 4 Datensätze unterschiedet. Schreiben Sie diese auf.


Ändern Sie nun in 3 weiteren Varianten die unterschiedlichen Entscheidungen und beurteilen Sie das Ergebnis.
    NDVI 0; 0.1; 0.2;